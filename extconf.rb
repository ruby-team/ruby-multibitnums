require "mkmf"

def have_type(type, header=nil)
  printf "checking for %s... ", type
  STDOUT.flush
  src = <<"SRC"
#include <ruby.h>
SRC
  unless header.nil?
  src << <<"SRC"
#include <#{header}>
SRC
  end
  r = try_link(src + <<"SRC")
  int main() { return 0; }
  int t() { #{type} a; return 0; }
SRC
  unless r
    print "no\n"
    return false
  end
  $defs.push(format("-DHAVE_%s", type.upcase))
  print "yes\n"
  return true
end

if have_header("sys/types.h")
  header = "sys/types.h"
else
  header = nil
end
have_type("int32_t", header)
create_makefile("numru/multibitnums")
