#-*- mode: ruby; encoding: utf-8-unix -*-#
require "test/unit"
require "narray"
require "numru/multibitnums"
include NumRu

class MultibitNumsTest2 < Test::Unit::TestCase
  def setup
    @nbit=10
    @testdata1 = ' '*6
    @testdata1[0] = 37.chr
    @testdata1[1] = 208.chr
    @testdata1[2] = 37.chr
    @testdata1[3] = 117.chr
    @testdata1[4] = 128.chr
    @testdata1[5] = 108.chr
    @expected1 = NArray[151,258,349,384]
    @testdata2 = ' '*6
    @testdata2[0] = 15.chr
    @testdata2[1] = 131.chr
    @testdata2[2] = 240.chr
    @testdata2[3] = 252.chr
    @testdata2[4] = 64.chr
    @testdata2[5] = 16.chr
    @expected2 = NArray[62,63,63,64]
  end

  define_method("test_bigendian_unsigned_int2") do
    mb = MultiBitNums.new(@testdata1, @nbit)
    assert_equal @expected1, NArray.to_na( mb.to_int32str, 'int' )
    mb = MultiBitNums.new(@testdata2, @nbit)
    assert_equal @expected2, NArray.to_na( mb.to_int32str, 'int' )
  end
end
