#-*- mode: ruby; encoding: utf-8-unix -*-#
require "test/unit"
require "narray"
require "numru/multibitnums"
include NumRu

class MultibitNumsTest1 < Test::Unit::TestCase

  def setup
    @testdata = ' '*10
    for i in 0..9
      @testdata[i] = 0x11.chr
    end

    @nbit2 = NArray.int(40).fill!(0)
    for i in 0..39
      @nbit2[i] = 1 if i%2 == 1
    end
    @nbit3 = NArray.int(26).fill!(0)
    for i in 0..25
      @nbit3[i] = 0 if i%4 == 0
      @nbit3[i] = 4 if i%4 == 1
      @nbit3[i] = 2 if i%4 == 2
      @nbit3[i] = 1 if i%4 == 3
    end
    @nbit4 = NArray.int(20).fill!(1)
    @nbit5 = NArray.int(16)
    for i in 0..15
      @nbit5[i] = 2  if i%4 ==0
      @nbit5[i] = 4  if i%4 ==1
      @nbit5[i] = 8  if i%4 ==2
      @nbit5[i] = 17 if i%4 ==3
    end
    @nbit6 = NArray.int(13)
    for i in 0..12
      @nbit6[i] = 4  if i%2 == 0
      @nbit6[i] = 17 if i%2 == 1
    end
    @nbit7 = NArray.int(11)
    for i in 0..10
      @nbit7[i] = 8  if i%4 == 0
      @nbit7[i] = 68 if i%4 == 1
      @nbit7[i] = 34 if i%4 == 2
      @nbit7[i] = 17 if i%4 == 3
    end
    @nbit8 = NArray.int(10).fill!(17)
    @nbit9 = NArray.int(8)
    for i in 0..7
      @nbit9[i] = 34  if i%4 == 0
      @nbit9[i] = 68  if i%4 == 1
      @nbit9[i] = 136 if i%4 == 2
      @nbit9[i] = 273 if i%4 == 3
    end
    @nbit10 = NArray.int(8)
    for i in 0..7
      @nbit10[i] = 68  if i%2 == 0
      @nbit10[i] = 273 if i%2 == 1
    end
  end

  define_method("test_bigedian_unsigned_int") do
    for nbit in 2..10
      mb = MultiBitNums.new(@testdata, nbit)
      assert_equal eval("@nbit#{nbit}"), NArray.to_na( mb.to_int32str, 'int')
    end
  end

end
